module.exponent = {
  devServer: {
    proxy: {
      "/*": {
        target: "http://localhost:81",
        ws: true,
        changeOrigin: true,
      },
    },
  },
};
