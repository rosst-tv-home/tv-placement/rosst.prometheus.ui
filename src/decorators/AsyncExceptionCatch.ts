import { createDecorator } from "vue-class-component";
import ComponentMixin from "../components/common/ComponentMixin";
import { Vue } from "vue-property-decorator";

/**
 * Метод логгирования исключений.
 *
 * @param {Vue} vue компонент.
 * @param {string} message сообщение об ошибке.
 */
function logException(vue: Vue, message: string) {
  if ((vue as ComponentMixin) !== undefined) {
    (vue as ComponentMixin).error("Ошибка", message);
  } else {
    // Если не содержит - выбрасываем исключение
    throw new Error(message);
  }
}

/**
 * Декоратор, отлавливающий исключения асинхронных http запросов. Вызывает окно оповещения с описанием ошибки, только
 * если компонент наследуюется от примеси с соответсвующими методами, иначе ошибки выводятся в консоль браузера.
 */
export const AsyncRequestCatchException = createDecorator((options, key) => {
  // Если нет контекста метода, на который применяется декоратор
  if (!options || !options.methods) {
    throw Error(
      'Ошибка декоратора "AsyncCatchDecorator", нет контекста метода.'
    );
  }

  // Оригинальное тело метода
  const method = options.methods[key];

  // Переопределяем тело метода
  options.methods[key] = async function wrapper(...args) {
    try {
      // Ожидаем асинхронного выполнения метода
      await method.call(this, args[0]);
    } catch (exception: any) {
      // Отлавливаем ошибки
      if (exception.response) {
        // Если есть ответ от сервера
        logException(this, exception.response.data);
      } else if (exception.request) {
        // Если нет ответа от сервера
        logException(this, "Нет ответа от сервера.");
      } else {
        // Если неизвестная ошибка
        logException(this, "Неизвестная ошибка");
      }
    }
  };
});
