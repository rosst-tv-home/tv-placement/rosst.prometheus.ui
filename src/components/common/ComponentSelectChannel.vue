<template>
  <el-select
    v-model="value"
    placeholder="Каналы"
    @change="onChange"
    :remote-method="search"
    :loading="isLoading"
    :disabled="isDisabled"
    :loading-text="'Поиск...'"
    :no-data-text="'Нет данных'"
    :no-match-text="'Совпадения не найдены'"
    remote
    :multiple="isMultiple"
    filterable
    collapse-tags
    clearable
  >
    <el-option
      v-for="channel in options"
      :key="channel.id"
      :label="channel.name"
      :value="channel.id"
    >
      <span class="id">{{ channel.id }}</span>
      <span class="name">{{ channel.name }}</span>
    </el-option>
  </el-select>
</template>

<script lang="ts">
import { Component, Mixins, Prop } from "vue-property-decorator";
import { Channel } from "@/interfaces/Channel";
import { AxiosInstance } from "@/modules/AxiosInstance";
import ComponentMixin from "@/components/common/ComponentMixin";
import { AsyncRequestCatchException } from "@/decorators/AsyncExceptionCatch";
import { SellingDirection } from "@/interfaces/SellingDirection";

/**
 * Компонент - поле выбора канала/каналов.
 */
@Component
export default class ComponentSelectChannel extends Mixins(ComponentMixin) {
  /**
   * @property {string} eventName - название события, которое инициализирует компонент.
   */
  private readonly eventName = "on-selected-channels";

  /**
   * @property {boolean} isMultiple - признак множественного выбора.
   */
  @Prop({ type: Boolean, required: true, default: true })
  protected readonly isMultiple!: boolean;

  /**
   * Вычисляемое свойство свойства выборанного(-ых) канала(-ов).
   */
  get value(): number[] | number | null {
    return this.isMultiple ? this.selectedIDs : this.selectedID;
  }

  /**
   * Setter вычисляемого свойства выбранного(-ых) канала(-ов).
   * @property {number[]|number|null} value значение свойства.
   */
  set value(value: number[] | number | null) {
    // Если признак множественного выбора
    if (this.isMultiple) {
      this.selectedIDs = value as number[];
    } else {
      this.selectedID = value as number;
    }
  }

  /**
   * Идентификатор выбранного канала.
   */
  private selectedID: number | null = null;

  /**
   * Коллекция идентификаторов выбранных каналов.
   */
  private selectedIDs: number[] = [];

  /**
   * Коллекция каналов.
   */
  private channels: Channel[] = [];

  /**
   * Признак "загрузка".
   */
  private isLoading = false;

  /**
   * Признак "неактивно".
   */
  private isDisabled = true;

  /**
   * Отфильтрованная коллекция каналов.
   */
  private options: Channel[] = [];

  /**
   * Метод срабатывает при создании компонента.
   */
  protected created(): void {
    // Подписываемся на событие изменения выбранного направления продаж
    this.subscribe(
      "on-selected-selling-direction",
      this.onSellingDirectionChanged
    );
  }

  /**
   * Метод срабатывает при изменении выбранного направления продаж.
   *
   * @param {SellingDirection|null} sellingDirection выбранное направление продаж.
   */
  private onSellingDirectionChanged(
    sellingDirection: SellingDirection | null
  ): void {
    // Если направление продаж не выбрано.
    if (!sellingDirection) {
      // Очищаем свойства компонента и завершаем выполнение метода
      return this.clearAll();
    }

    // Иначе отправляем запрос на сервер
    this.getChannelsBySellingDirectionID(sellingDirection);
  }

  /**
   * Метод запроса списка каналов по указанному направлению продаж.
   *
   * @param {SellingDirection} sellingDirection выбранного направления продаж.
   */
  @AsyncRequestCatchException
  private async getChannelsBySellingDirectionID(
    sellingDirection: SellingDirection
  ): Promise<void> {
    // Переключаем флаг "загрузка"
    this.isLoading = true;

    // Отправляем запрос на сервер
    const request = AxiosInstance.get(
      "media-plan-search-view/get-channels-by-selling-direction",
      { params: { id: sellingDirection.id } }
    );

    // Очищаем свойства компонента
    this.clearAll();

    // Ожидаём ответа на запрос
    const response = await request;

    // Переключаем флаг "загрузка"
    this.isLoading = false;

    // Если нет данных - завершаем выполнение метода
    if (!response.data) return;
    if (!Array.isArray(response.data)) return;

    // Сохраняем новую коллекцию каналов
    this.channels = (response.data as Channel[]).sort((a, b) =>
      a.name.localeCompare(b.name)
    );
    this.options = this.channels;

    // Делаем компонент активным
    this.isDisabled = false;

    // Делаем все каналы выбранными
    this.selectedIDs = this.channels.map((channel) => channel.id);
    this.onChange();
  }

  /**
   * Метод срабатывает после изменения выбранных каналов.
   */
  private onChange(): void {
    if (this.isMultiple) {
      // Выбираем из общего списка выбранные каналы
      const selectedChannels = this.channels.filter((channel) =>
        this.selectedIDs.includes(channel.id)
      );
      // Оповещаем другие компоненты об изменении коллекции выбранных каналов
      this.notify(this.eventName, selectedChannels);
    } else {
      const selectedChannel = this.channels.find(
        (channel) => this.selectedID === channel.id
      );
      this.notify(
        this.eventName,
        selectedChannel !== undefined ? selectedChannel : null
      );
    }
  }

  /**
   * Метод срабатывает после ввода символов в поле поиска.
   *
   * @param {string} text содержимое поля поиска.
   */
  private search(text: string): void {
    this.isLoading = true;
    setTimeout(() => {
      this.options = this.channels.filter(
        (channel) =>
          channel.name.toLowerCase().indexOf(text.toLowerCase()) !== -1
      );
      this.isLoading = false;
    }, 200);
  }

  /**
   * Метод очистки свойств компонента.
   */
  private clearAll(): void {
    // Делаем компонент неактивным
    this.isDisabled = true;
    // Очищаем коллекцию каналов
    this.channels = [];
    // Очищаем коллекцию отфильтрованный каналов
    this.options = [];
    // Очищаем идентификатор выбранного канала
    this.selectedID = null;
    // Очищаем коллекцию идентификаторов выбранных каналов
    this.selectedIDs = [];
    // Оповещаем о том что каналы не выбраны
    this.notify(this.eventName, null);
  }
}
</script>
<style lang="scss" scoped>
.el-select {
  width: 100%;
  margin-bottom: 2%;
}
</style>
