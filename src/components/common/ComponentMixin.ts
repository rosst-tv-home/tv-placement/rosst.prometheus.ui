import { Component, Prop, Vue } from "vue-property-decorator";
import { ElNotificationComponent } from "element-ui/types/notification";

/**
 * Примесь - компонент подписчик на автобус событий.
 */
@Component
export default class ComponentMixin extends Vue {
  /**
   * @property {Vue} event_bus - автобус событий.
   */
  @Prop({ type: Vue, required: true })
  protected readonly eventBus!: Vue;

  /**
   * @property {Map<string, Function>} subscribes - таблица названий событий и их методов - обработчиков.
   */
  private subscribes: Map<string, (...args: never[]) => void> = new Map<
    string,
    (...args: never[]) => void
  >();

  /**
   * Метод добавления подписки на событие.
   *
   * @param {string} eventName - название события.
   * @param {Function} handlerFunction - метод обработки события.
   */
  protected subscribe(
    eventName: string,
    handlerFunction: (...args: never[]) => void
  ): void {
    this.addSubscription(eventName, handlerFunction);
    this.eventBus.$on(eventName, handlerFunction);
  }

  /**
   * Метод добавления подписок на события.
   *
   * @param {Array<Array<string, Function>>} map - таблица названий событий и их методов - обработчиков.
   */
  protected subscribeAll(map: Array<[string, (...args: never) => void]>): void {
    map.forEach((entry) => {
      this.addSubscription(entry[0], entry[1]);
      this.eventBus.$on(entry[0], entry[1]);
    });
  }

  /**
   * Метод оповещения подписчиков о событии.
   *
   * @param {string} eventName - название события.
   * @param {any} payload - данные события.
   */
  protected notify(eventName: string, ...payload: unknown[]): void {
    this.eventBus.$emit(eventName, ...payload);
  }

  /**
   * Метод добавления подписки в список подписок. Удаляет старую подписку на событие, если существует.
   *
   * @param {string} eventName - название события.
   * @param {Function} handlerFunction - метод обработки события.
   */
  private addSubscription(
    eventName: string,
    handlerFunction: (...args: never[]) => void
  ): void {
    // Поиск метода обработки события в списке подписок
    const subscriptionHandlerFunction = this.subscribes.get(eventName);
    // Если метод обработки найден, значит компонент подписан на событие
    if (subscriptionHandlerFunction) {
      // Отписываемся от старого события (тем самым меняем старый метод обработки на новый)
      this.eventBus.$off(eventName, subscriptionHandlerFunction);
      // Удаляем старую подписку из списка подписок
      this.subscribes.delete(eventName);
    }
    // Добавляем новую подписку в список подписок
    this.subscribes.set(eventName, handlerFunction);
  }

  /**
   * Метод вызывается перед уничтожением компонента.
   */
  protected beforeDestroy(): void {
    // Отписываемся от всех событий
    this.subscribes.forEach((value, key) => {
      this.eventBus.$off(key, value);
    });
    // Очищаем список подписок
    this.subscribes.clear();
  }

  /**
   * Метод оповещения об ошибке.
   *
   * @param {string} title - заголовок оповещения.
   * @param {string} message - сообщение оповещения.
   * @param {number} duration - длительность отображения оповещения.
   *
   * @return {ElNotificationComponent} экземпляр компонента оповещения.
   */
  public error(
    title: string,
    message: string,
    duration = 0
  ): ElNotificationComponent {
    return this.$notify({
      type: "error",
      title: title,
      message: message,
      duration: duration,
    });
  }

  /**
   * Метод оповещения о предупреждении.
   *
   * @param {string} title - заголовок оповещения.
   * @param {string} message - сообщение оповещения.
   * @param {number} duration - длительность отображения оповещения.
   *
   * @return {ElNotificationComponent} экземпляр компонента оповещения.
   */
  public warning(
    title: string,
    message: string,
    duration = 0
  ): ElNotificationComponent {
    return this.$notify({
      type: "warning",
      title: title,
      message: message,
      duration: duration,
    });
  }
}
