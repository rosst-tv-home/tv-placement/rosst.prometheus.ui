import Vue from "vue";
// Главное представление
import Application from "./Application.vue";
// Модули приложения
import { StoreInstance } from "./modules/StoreInstance";
import { RouterInstance } from "./modules/RouterInstance";
// CSS Framework
import ElementUI from "element-ui";
// Стиль CSS Framework
import "element-ui/lib/theme-chalk/index.css";
// Локаль CSS Framework
import ElementUILocale from "element-ui/lib/locale/lang/ru-RU";
// Библиотека тура по приложению
import "driver.js/dist/driver.min.css";

// Подключаем CSS Framework и устанавливаем локаль
Vue.use(ElementUI, { locale: ElementUILocale });
Vue.config.productionTip = false;

// Создаём новый экземпляр приложения и монтируем в тег с id="root"
new Vue({
  router: RouterInstance,
  store: StoreInstance,
  render: (h) => h(Application),
}).$mount("#root");
