import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export const StoreInstance = new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {},
});
