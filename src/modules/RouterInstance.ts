import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

// Маршруты приложения
const routes = [
  // Представление поиска медиаплана
  {
    path: "/",
    name: "ViewMediaPlanSearch",
    component: () => import("@/views/ViewMediaPlanSearch.vue"),
  },
  {
    path: "/base-channels-audiences",
    name: "ViewChannelBaseAudience",
    component: () => import("@/views/ViewChannelBaseAudience.vue"),
  },
  // Представление параметров размещения медиаплана
  {
    path: "/:id",
    name: "ViewMediaPlanParameters",
    component: () => import("@/views/ViewMediaPlanParameters.vue"),
  },
];

// Конфигурация маршрутизатора приложения
const RouterInstance = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export { RouterInstance };
