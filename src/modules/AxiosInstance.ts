import axios from "axios";

/**
 * @property {string} BASE_URL - адрес запросов.
 */
const BASE_URL =
  process.env.NODE_ENV == "production"
    ? "http://palomars:81"
    : "http://localhost:81";
//   "http://192.168.0.151:81";
//   "http://palomars:81"

/**
 * @property {AxiosInstance} AxiosInstance - экземпляр модуля axios.
 */
const AxiosInstance = axios.create({ baseURL: BASE_URL });

AxiosInstance.interceptors.request.use((configuration) => {
  configuration.headers["Content-Type"] = "application/json";
  configuration.headers.Authorization =
    "Bearer eyJhbGciOiJIUzUxMiJ9." +
    "eyJzdWIiOiJpbXVsbGlrYTEifQ." +
    "I8fZOS0SW5AO94gT2yoIlJt5nR7" +
    "WqfSO5SrtInKpay73FLPo20mjT3LIN" +
    "gls_TJgAJvazv1VzPq-J62wxAj0Eg";
  return configuration;
});

export { AxiosInstance };
