import { Channel } from "@/interfaces/Channel";
import { InterfaceOrder, Order } from "@/interfaces/Order";
import { Film } from "@/interfaces/Film";

/**
 * @interface InterfaceMediaPlan Интерфейс медиаплана.
 */
interface InterfaceMediaPlan extends ReadableStream {
  /**
   * @property {number} id Идентификатор медиаплана.
   */
  id: number;
  /**
   * @property {string} name Название медиаплана.
   */
  name: string;
  /**
   * @property {Date} month Месяц медиаплана.
   */
  month: Date;
  /**
   * @property {InterfaceOrder} order Интерфейс заказа, к которому относится медиаплан.
   */
  order: InterfaceOrder;
  /**
   * @property {Channel} channel Канал, к которому относится медиаплан.
   */
  channel: Channel;
  /**
   * @property {Film[]} films Коллекция роликов медиаплана.
   */
  films: Film[];
  /**
   * @property {null | number} difference Разница между плановым значение инвентаря медиаплана и фактическим.
   */
  difference: null | number;

  stopPlacement: boolean;
}

/**
 * @class Модель медиаплана.
 */
class MediaPlan {
  /**
   * @property {number} id Идентификатор медиаплана.
   */
  id: number;
  /**
   * @property {string} name Название медиаплана.
   */
  name: string;
  /**
   * @property {Date} month Месяц медиаплана.
   */
  month: Date;
  /**
   * @property {Order} order Заказ, к которому относится медиаплан.
   */
  order: Order;
  /**
   * @property {Channel} channel Канал, к которому относится медиаплан.
   */
  channel: Channel;
  /**
   * @property {Film[]} films Коллекция роликов медиаплана.
   */
  films: Film[];
  /**
   * @property {null | number} difference Разница между плановым значением инвентаря медиаплана и фактическим.
   */
  readonly difference: null | number = null;

  isPlacementActive: boolean;

  hasParameters = false;

  /**
   * @constructor Конструктор.
   * @param {InterfaceMediaPlan} mediaPlan Интерфейс медиаплана.
   */
  constructor(mediaPlan: InterfaceMediaPlan) {
    this.id = mediaPlan.id;
    this.name = mediaPlan.name;
    this.channel = mediaPlan.channel;

    this.month = new Date(mediaPlan.month);
    this.order = new Order(mediaPlan.order);
    this.films = mediaPlan.films;
    this.difference = mediaPlan.difference;
    this.isPlacementActive = mediaPlan.stopPlacement;
  }
}

export { InterfaceMediaPlan, MediaPlan };
