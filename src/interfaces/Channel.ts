/**
 * @interface Channel - интерфейс канала.
 */
export interface Channel {
  /**
   * @property {number} id - идентификатор канала.
   */
  id: number;
  /**
   * @property {string} name - название канала.
   */
  name: string;
  /**
   * @property {number} palomarsChannelID - идентификатор канала Palomaras.
   */
  palomarsChannelID: null | number;
}
