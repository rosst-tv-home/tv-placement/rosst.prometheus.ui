/**
 * @interface InterfaceMediaPlanParametersPeriodFilm интерфейс ролика периода параметров автоматической расстановки медиаплана.
 *
 * @property {number} id                идентификатор.
 * @property {number} linkID            идентификатор ролика.
 * @property {number} inventoryPercent  процентр инвентаря ролика от инвентаря периода.
 * @property {Date}   dateFrom          дата начала периода размещения ролика.
 * @property {Date}   dateBefore        дата окончания периода размещения ролика.
 */
interface InterfaceMediaPlanParametersPeriodFilm {
  id: number;
  linkID: number;
  duration: number;
  versionName: string;
  inventoryPercent: number;
  dateFrom: Date;
  dateBefore: Date;
}

/**
 * @class класс ролика периода параметров автоматической расстановки медиаплана.
 *
 * @property {number} id                идентификатор.
 * @property {number} linkID            идентификатор ролика.
 * @property {number} inventoryPercent  процентн инвентаря ролика от инвентаря периода.
 * @property {Date}   dateFrom          дата начала периода размещения ролика.
 * @property {Date}   dateBefore        дата окончания периода размещения ролика.
 */
class MediaPlanParametersPeriodFilm {
  readonly id: number;
  readonly linkID: number;
  duration: number;
  versionName: string;
  inventoryPercent: number;
  dateFrom: Date;
  dateBefore: Date;

  /**
   * @constructor конструктор.
   * @param {InterfaceMediaPlanParametersPeriodFilm} film интерфейс ролика периода параметров автоматической расстановки медиаплана.
   */
  constructor(film: InterfaceMediaPlanParametersPeriodFilm) {
    this.id = film.id;
    this.linkID = film.linkID;
    this.duration = film.duration;
    this.versionName = film.versionName;
    this.inventoryPercent = film.inventoryPercent;
    this.dateFrom = new Date(film.dateFrom);
    this.dateBefore = new Date(film.dateBefore);
  }
}

export {
  MediaPlanParametersPeriodFilm,
  InterfaceMediaPlanParametersPeriodFilm,
};
