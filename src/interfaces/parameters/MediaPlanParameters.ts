import {
  InterfaceMediaPlanParametersTime,
  MediaPlanParametersTime,
} from "@/interfaces/parameters/MediaPlanParametersTime";
import {
  InterfaceMediaPlanParametersPeriod,
  MediaPlanParametersPeriod,
} from "@/interfaces/parameters/MediaPlanParametersPeriod";
import { MediaPlanParametersPlacementType } from "@/interfaces/parameters/MediaPlanParametersPlacementType";
import { InterfaceTvrParam, TvrParam } from "@/interfaces/parameters/TvrParam";

/**
 * @interface InterfaceMediaPlanParameters интерфейс параметров автоматического размещения медиаплана.
 *
 * @property {number} id идентификатор.
 * @property {number} mediaPlanID идентификатор медиаплана.
 * @property {InterfaceMediaPlanParametersTime[]} times временные периоды.
 * @property {InterfaceMediaPlanParametersPeriod[]} periods периоды.
 */
interface InterfaceMediaPlanParameters {
  readonly id: number;
  readonly mediaPlanID: number;
  times: InterfaceMediaPlanParametersTime[];
  periods: InterfaceMediaPlanParametersPeriod[];
  audience: TvrParam | null;
  placement: MediaPlanParametersPlacementType | null;
  readonly percentInventoryNeed: number;
  isPlacementActive: boolean;
  isAssignedRatingsPlacement: boolean;
}

/**
 * @class Класс параметров автоматического размещения медиаплана.
 *
 * @property {number} id идентификатор.
 * @property {number} mediaPlanID идентификатор медиаплана.
 * @property {Array<MediaPlanParametersTime>} times коллекция временных периодов.
 * @property {Array<MediaPlanParametersPeriod>} periods коллекция периодов.
 */
class MediaPlanParameters {
  readonly id: number;
  readonly mediaPlanID: number;
  times: Array<MediaPlanParametersTime>;
  periods: Array<MediaPlanParametersPeriod>;
  audience: TvrParam | null = null;
  placement: MediaPlanParametersPlacementType | null = null;
  percentInventoryNeed: number;
  isPlacementActive: boolean;
  isAssignedRatingsPlacement: boolean;

  /**
   * @constructor конструктор.
   *
   * @param {InterfaceMediaPlanParameters} parameters интерфейс параметров автоматического размещения медиаплана.
   */
  constructor(parameters: InterfaceMediaPlanParameters) {
    this.id = parameters.id;
    this.mediaPlanID = parameters.mediaPlanID;
    this.times = parameters.times.map(
      (time) => new MediaPlanParametersTime(time)
    );
    this.periods = parameters.periods.map(
      (period) => new MediaPlanParametersPeriod(period)
    );
    if (parameters.audience) {
      this.audience = new TvrParam(parameters.audience as InterfaceTvrParam);
    }
    this.placement = parameters.placement;
    this.percentInventoryNeed = parameters.percentInventoryNeed;
    this.isPlacementActive = parameters.isPlacementActive;
    this.isAssignedRatingsPlacement = parameters.isAssignedRatingsPlacement;
  }
}

export { MediaPlanParameters, InterfaceMediaPlanParameters };
