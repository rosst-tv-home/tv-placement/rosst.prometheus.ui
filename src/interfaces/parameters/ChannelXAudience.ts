import { PalomarsChannel } from "@/interfaces/PalomarsChannel";
import { Audience } from "@/interfaces/Audience";

/**
 * @interface InterfaceChannelXAudience связей базовой аудитории канала.
 *
 * @property {Date} date даты.
 */
interface InterfaceChannelXAudience {
  audience: Audience;
  date: Date;
  palomarsChannel: PalomarsChannel;
}
/**
 * @class Класс связей базовой аудитории канала.
 *
 * @property {Date} date дата.
 */
class ChannelXAudience {
  audience: Audience;
  date: Date;
  palomarsChannel: PalomarsChannel;
  /**
   * @constructor конструктор.
   *
   * @param {InterfaceChannelXAudience} parameter интерфейс  связей базовой аудитории канала.
   */
  constructor(parameter: InterfaceChannelXAudience) {
    this.date = new Date(parameter.date);
    this.audience = parameter.audience as Audience;
    this.palomarsChannel = parameter.palomarsChannel as PalomarsChannel;
  }
}
export { InterfaceChannelXAudience, ChannelXAudience };
