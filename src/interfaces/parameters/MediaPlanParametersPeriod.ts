import {
  InterfaceMediaPlanParametersPeriodFilm,
  MediaPlanParametersPeriodFilm,
} from "@/interfaces/parameters/MediaPlanParametersPeriodFilm";

/**
 * @interface InterfaceMediaPlanParametersPeriod Интерфейс периода параметров автоматического размещения медиаплана.
 *
 * @property {number} id идентификатор.
 * @property {null | Date} fromDate дата начала периода.
 * @property {null | Date} beforeDate дата окончания периода.
 * @property {number} inventory инвентарь периода.
 * @property {number} percentPrime процентн prime от инвентаря периода.
 * @property {Array<InterfaceMediaPlanParametersPeriodFilm>} коллекция роликов периода.
 */
interface InterfaceMediaPlanParametersPeriod {
  id: number;
  fromDate: null | Date;
  beforeDate: null | Date;
  inventory: number;
  percentPrime: number;
  films: Array<InterfaceMediaPlanParametersPeriodFilm>;
}

/**
 * @class Класс периода параметров автоматического размещения медиаплана.
 *
 * @property {number} id идентификатор.
 * @property {null | Date} fromDate дата начала периода.
 * @property {null | Date} beforeDate дата окончания периода.
 * @property {number} inventory инвентарь периода.
 * @property {number} percentPrime процентн prime от инвентаря периода.
 * @property {Array<MediaPlanParametersPeriodFilm>} коллекция роликов периода.
 */
class MediaPlanParametersPeriod {
  id: number;
  fromDate: null | Date = null;
  beforeDate: null | Date = null;
  periods: Array<Date> = [];
  inventory: string | number;
  percentPrime: number;
  films: Array<MediaPlanParametersPeriodFilm>;
  filteredFilms: Array<MediaPlanParametersPeriodFilm> = [];

  /**
   * @constructor конструктор.
   *
   * @param {InterfaceMediaPlanParametersPeriod} period интерфейс периода параметров автоматического размещения медиаплана.
   */
  constructor(period: InterfaceMediaPlanParametersPeriod) {
    this.id = period.id;
    if (period.fromDate) {
      this.periods.push(new Date(period.fromDate));
      this.fromDate = new Date(period.fromDate);
    }
    if (period.beforeDate) {
      this.periods.push(new Date(period.beforeDate));
      this.beforeDate = new Date(period.beforeDate);
    }
    this.inventory = period.inventory == 0.0 ? "" : period.inventory;
    this.percentPrime = period.percentPrime;
    this.films = period.films.map(
      (film) => new MediaPlanParametersPeriodFilm(film)
    );
    if (!this.fromDate || !this.beforeDate) this.filteredFilms = this.films;
  }

  /**
   * Метод формирования отфильтрованного списка роликов.
   */
  public filterFilms(
    onOff: boolean
  ): MediaPlanParametersPeriodFilm[] | undefined {
    if (!this.fromDate || !this.beforeDate || !onOff)
      return (this.filteredFilms = this.films.sort(
        (a, b) => b.duration - a.duration
      ));
    this.filteredFilms = this.films
      .filter((film) => {
        return (
          film.dateFrom.getFullYear() == this.fromDate?.getFullYear() &&
          film.dateFrom.getMonth() == this.fromDate.getMonth() &&
          film.dateFrom.getDate() >= this.fromDate.getDate() &&
          film.dateBefore.getFullYear() == this.beforeDate?.getFullYear() &&
          film.dateBefore.getMonth() == this.beforeDate.getMonth() &&
          film.dateBefore.getDate() <= this.beforeDate.getDate()
        );
      })
      .sort((a, b) => b.duration - a.duration);
  }
}

export { MediaPlanParametersPeriod, InterfaceMediaPlanParametersPeriod };
