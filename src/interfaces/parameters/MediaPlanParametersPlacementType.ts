/**
 * @class перечисляемый класс типов размещения медиаплана.
 *
 * @property AFFINITIES {MediaPlanParametersPlacementType.AFFINITIES} расстановка медиаплана по аффинити.
 * @property EQUAL_DISTRIBUTION {MediaPlanParametersPlacementType.EQUAL_DISTRIBUTION} расстановка медиаплана равномерно.
 */
export enum MediaPlanParametersPlacementType {
  AFFINITIES = "AFFINITIES",
  EQUAL_DISTRIBUTION = "EQUAL_DISTRIBUTION",
}
