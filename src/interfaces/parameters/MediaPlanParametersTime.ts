/**
 * @interface InterfaceMediaPlanParametersTime интерфейс временных периодов.
 *
 * @property {number}   id          идентификатор.
 * @property {Date}     timeFrom    дата начала временного периода.
 * @property {Date}     timeBefore  дата окончания временного периода.
 * @property {boolean}  isWeekend   признак "временной период выходного дня".
 */
interface InterfaceMediaPlanParametersTime {
  readonly id: number;
  readonly timeFrom: Date;
  readonly timeBefore: Date;
  readonly isWeekend: boolean;
}

/**
 * @class MediaPlanParametersTime класс временного периода.
 *
 * @property {number}   id          идентификатор.
 * @property {Date}     timeFrom    время начала временного периода.
 * @property {Date}     timeBefore  время окончания временного периода.
 * @property {boolean}  isWeekend   признак "временной период выходного дня".
 */
class MediaPlanParametersTime {
  readonly id: number;
  readonly timeFrom: Date;
  readonly timeBefore: Date;
  readonly isWeekend: boolean;

  /**
   * @constructor Конструктор.
   *
   * @param {InterfaceMediaPlanParametersTime} time интерфей временного периода.
   */
  constructor(time: InterfaceMediaPlanParametersTime) {
    this.id = time.id;
    this.timeFrom = new Date(time.timeFrom);
    this.timeBefore = new Date(time.timeBefore);
    this.isWeekend = time.isWeekend;
  }

  /**
   * Метод проверки равенства объектов временных периодов.
   *
   * @param {MediaPlanParametersTime} other другой временной период.
   *
   * @return {boolean} признак равенства двух объектов.
   */
  equals(other: MediaPlanParametersTime): boolean {
    if (this.timeFrom.getHours() != other.timeFrom.getHours()) return false;
    if (this.timeFrom.getMinutes() != other.timeFrom.getMinutes()) return false;
    if (this.timeBefore.getHours() != other.timeBefore.getHours()) return false;
    if (this.timeBefore.getMinutes() != other.timeBefore.getMinutes())
      return false;
    return this.isWeekend == other.isWeekend;
  }
}

export { MediaPlanParametersTime, InterfaceMediaPlanParametersTime };
