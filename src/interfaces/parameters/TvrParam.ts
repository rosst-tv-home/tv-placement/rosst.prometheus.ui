interface InterfaceTvrParam {
  readonly date: Date;
  readonly name: string;
}

class TvrParam {
  readonly date: null | Date = null;
  readonly name: null | string = null;
  readonly key: string;

  constructor(parameters: InterfaceTvrParam) {
    this.date = new Date(parameters.date);
    this.name = parameters.name;
    this.key = this.name + this.date.getFullYear() + this.date.getMonth();
  }
}

export { InterfaceTvrParam, TvrParam };
