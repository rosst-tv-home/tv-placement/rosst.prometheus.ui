import { Advertiser } from "@/interfaces/Advertiser";
import { SellingDirection } from "@/interfaces/SellingDirection";

/**
 * @interface InterfaceAgreement - интерфейс модели сделки.
 */
interface InterfaceAgreement {
  /**
   * @property {number} id - идентификатор сделки.
   */
  id: number;
  /**
   * @property {string} name - название сделки.
   */
  name: string;
  /**
   * @property {Advertiser} advertiser - рекламодатель.
   */
  advertiser: Advertiser;
  /**
   * @property {SellingDirection} sellingDirection - направление продаж, к которому относится сделка.
   */
  sellingDirection: SellingDirection;
}

/**
 * @class Модель сделки.
 */
class Agreement {
  /**
   * @property {number} id идентификатор сделки.
   */
  id: number;
  /**
   * @property {string} name название сделки.
   */
  name: string;
  /**
   * @property {Advertiser} рекламодатель, к которому относится сделка.
   */
  advertiser: Advertiser;
  /**
   * @property {SellingDirection} sellingDirection - направление продаж, к которому относится сделка.
   */
  sellingDirection: SellingDirection;

  /**
   * @constructor Конструктор модели сделки.
   * @param {InterfaceAgreement} agreement интерфейс сделки.
   */
  constructor(agreement: InterfaceAgreement) {
    this.id = agreement.id;
    this.name = agreement.name;
    this.advertiser = agreement.advertiser;
    this.sellingDirection = agreement.sellingDirection;
  }
}

export { InterfaceAgreement, Agreement };
