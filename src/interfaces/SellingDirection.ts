/**
 * @interface SellingDirection - интерфейс направления продаж.
 */
export interface SellingDirection {
  /**
   * @property {number} id - идентификатор направления продаж.
   */
  id: number;
  /**
   * @property {string} name - название направления продаж.
   */
  name: string;
}
