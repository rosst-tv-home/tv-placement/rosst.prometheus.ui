class IsPlacementActive {
  id: number;
  isPlacementActive: boolean;
  constructor(id: number, isPlacementActive: boolean) {
    this.id = id;
    this.isPlacementActive = isPlacementActive;
  }
}

export { IsPlacementActive };
