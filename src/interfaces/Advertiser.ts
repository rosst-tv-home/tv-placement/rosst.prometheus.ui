/**
 * Интерфейс модели рекламодателя.
 *
 * @interface Advertiser
 * @property {number} id    - Идентификатор рекламодателя.
 * @property {string} name  - Наименование рекламодателя.
 */
export interface Advertiser {
  /**
   * @property {number} id - идентификатор рекламодателя.
   */
  id: number;
  /**
   * @property {string} name - название рекламодателя.
   */
  name: string;
}
