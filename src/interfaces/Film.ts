/**
 * @interface Film Интерфейс ролика.
 */
export interface Film {
  /**
   * @property {number} id Идентификатор ролика.
   */
  id: number;
  /**
   * @property {number} linkID Идентификатор связи ролика с медиапланом.
   */
  linkID: number;
  /**
   * @property {string} name Название ролика.
   */
  name: string;
  /**
   * @property {string} versionName Название версии ролика.
   */
  versionName: string;
  /**
   * @property {number} duration Длительность ролика.
   */
  duration: number;
  /**
   * @property {boolean} isInLaw Признак ролика "соответсвует закону о рекламе".
   */
  isInLaw: boolean;
  /**
   * @property {Date} dateFrom Дата начала периода размещения ролика.
   */
  dateFrom: Date;
  /**
   * @property {Date} dateBefore Дата окончания периода размещения ролика.
   */
  dateBefore: Date;
}
