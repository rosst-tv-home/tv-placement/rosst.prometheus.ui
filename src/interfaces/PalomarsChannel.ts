/**
 * @interface PalomarsChannel Интерфейс канала.
 */
export interface PalomarsChannel {
  /**
   * @property {number} id Идентификатор канала.
   */
  id: number;
  /**
   * @property {string} название канала.
   */
  name: string;
}
