import { Agreement, InterfaceAgreement } from "@/interfaces/Agreement";

/**
 * @interface InterfaceOrder Интерфейс заказа.
 */
interface InterfaceOrder {
  /**
   * @property {number} id Идентификатор заказа.
   */
  id: number;
  /**
   * @property {string} name Название заказа.
   */
  name: string;
  /**
   * @property {boolean} isSuperFix Признак заказа "Super Fix размещение".
   */
  isSuperFix: boolean;
  /**
   * @property {InterfaceAgreement} agreement Интерфейс сделки, к которой относится заказ.
   */
  agreement: InterfaceAgreement;
}

/**
 * @class Модель заказа.
 */
class Order {
  /**
   * @property {number} id Идентификатор заказа.
   */
  id: number;
  /**
   * @property {string} name Название заказа.
   */
  name: string;
  /**
   * @property {boolean} isSuperFix Признак заказа "Super Fix размещение".
   */
  isSuperFix: boolean;
  /**
   * @property {Agreement} agreement Сделка, к которой относится заказ.
   */
  agreement: Agreement;

  /**
   * @constructor Конструктор.
   * @param {InterfaceOrder} order Интерфейс заказа.
   */
  constructor(order: InterfaceOrder) {
    this.id = order.id;
    this.name = order.name;
    this.isSuperFix = order.isSuperFix;
    this.agreement = new Agreement(order.agreement);
  }
}

export { InterfaceOrder, Order };
