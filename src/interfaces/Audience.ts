/**
 * @interface Audience - интерфейс аудитории.
 */
export interface Audience {
  /**
   * @property {number} id - идентификатор аудитории.
   */
  id: number;
  /**
   * @property {string} name - название аудитории.
   */
  name: string;
}
